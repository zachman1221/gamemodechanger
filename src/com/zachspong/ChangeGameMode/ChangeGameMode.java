package com.zachspong.ChangeGameMode;

import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.ServerOperator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;


public class ChangeGameMode extends JavaPlugin {

    public int y;
    public Player playa;
    public Player checkname;
    public String matchednames = "";
    public int amt = 0;
    public String amtp;
    public int x;
    public String oplayer;
    public String namn = ChatColor.GOLD + " ";
    public Boolean found;
    public Player dude;
    public Plugin instance;
    static final Logger log = Logger.getLogger("Minecraft");
	public final Logger logger = Logger.getLogger("Minecraft");



    public void onDisable() {
		PluginDescriptionFile pdFile = this.getDescription();
		this.logger.info(pdFile.getName() + " version " + pdFile.getVersion() + " is Disabled");
    }

    public void onEnable() {
		PluginDescriptionFile pdFile = this.getDescription();
		this.logger.info(pdFile.getName() + " version " + pdFile.getVersion() + " is Enabled");
    }

    public boolean hasPerms(Player player, String perm) {
        String perms;
        if(!player.isOp()){
        if (!player.hasPermission("gm.*")) {
            if ("gm.creative".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms)) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            if ("gm.survival".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms)) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            if ("gm.creative.other".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms)) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            if ("gm.survival.other".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms)) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            if ("gm.toggle".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms) || player.hasPermission("gm.toggle.*")) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            if ("gm.toggle.other".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms) || player.hasPermission("gm.toggle.*")) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            if ("gm.help".equals(perm)) {
                perms = perm;
                if (player.hasPermission(perms)) {
                    return true;
                } else {
                    NoPerms(player);
                    return false;
                }
            }
            NoPerms(player);
            return false;

        } else 
            return true;
        } else
            return true;
    }

    public void Creative(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (args.length < 1) {
            if (hasPerms(player, "gm.creative")) {
                player.setGameMode(GameMode.CREATIVE);
                sender.sendMessage(namn + "You changed your game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
                log.info("[GameMode] " + player.getDisplayName() + " changed game mode to Creative!");
            }
        } else if (args.length == 1) {
            if (hasPerms(player, "gm.creative.other")) {
                if (isOnline(sender, args)) {
                    dude.setGameMode(GameMode.CREATIVE);
                    sender.sendMessage(namn + "You changed " + ChatColor.DARK_RED + dude.getDisplayName() + ChatColor.GOLD + "'s game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
                    dude.sendMessage(namn + ChatColor.DARK_RED + player.getDisplayName() + ChatColor.GOLD + " changed your game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
                    log.info("[GameMode] " + player.getDisplayName() + " changed " + dude.getDisplayName() + "'s game mode to Creative!");
                }
            }
        }
    }

    public boolean isOnline(CommandSender sender, String[] args) {
        Player list[] = sender.getServer().getOnlinePlayers();
        found = false;
        matchednames = "";
        checkname = null;
        amt = 0;
        if ("t".equals(args[0]) || "toggle".equals(args[0])) {
            x = 1;
        } else {
            x = 0;
        }
        for (Player checkname : list) {
            if (checkname.getDisplayName().toLowerCase().contains(args[x].toLowerCase())) {
                amt++;
                playa = checkname;
                matchednames += ChatColor.DARK_RED + checkname.getDisplayName() + ChatColor.GOLD + ", ";
            }
        }
        if (amt == 1) {
            dude = playa;
            found = true;
        }
        if (found) {
            return true;
        } else if (!found && amt == 0) {
            sender.sendMessage(namn + "Unable to find player " + ChatColor.DARK_RED + args[x] + ChatColor.GOLD + "!");
            return false;
        } else if (!found && amt > 1) {
            sender.sendMessage(namn + "Too many names matched!");
            sender.sendMessage(namn + "String entered: " + ChatColor.DARK_RED + args[x]);
            sender.sendMessage(namn + "Names matched: " + matchednames);
            return false;
        }
        return false;
    }

    public void NoPerms(CommandSender sender) {
        y++;
        if (y == 1) {
            sender.sendMessage(namn + ChatColor.RED + "You don't have permissions to do that!");
        }
    }

    public void Survival(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (args.length < 1) {
            if (hasPerms(player, "gm.survival")) {
                player.setGameMode(GameMode.SURVIVAL);
                sender.sendMessage(namn + "You changed your game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
                log.info("[GameMode] " + player.getDisplayName() + " changed game mode to Survival!");
            }
        } else if (args.length == 1) {
            if (hasPerms(player, "gm.survival.other")) {
                if (isOnline(sender, args)) {
                    dude.setGameMode(GameMode.SURVIVAL);
                    sender.sendMessage(namn + "You changed " + ChatColor.DARK_RED + dude.getDisplayName() + ChatColor.GOLD + "'s game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
                    dude.sendMessage(namn + ChatColor.DARK_RED + player.getDisplayName() + ChatColor.GOLD + " changed your game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
                    log.info("[GameMode] " + player.getDisplayName() + " changed " + dude.getDisplayName() + "'s game mode to Survival!");
                }
            }
        }
    }

    public void Console(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("gm") && args.length != 0) {
            if (label.equalsIgnoreCase("gm") && !args[0].equalsIgnoreCase("t") && !args[0].equalsIgnoreCase("toggle") && !args[0].equalsIgnoreCase("t") && !args[0].equalsIgnoreCase("h") && !args[0].equalsIgnoreCase("help")) {
                log.info("[GameMode] Unknown Command!");
            }
        }
        if (args.length > 2) {
            log.info("[GameMode] Too many arguments!");
        }
        if (args.length < 1) {
            sendCHelp();
        }
        if (args.length > 0 && label.equalsIgnoreCase("gm") && (args[0].equalsIgnoreCase("h") || args[0].equalsIgnoreCase("help"))) {
            sendCHelp();
        }

        if (label.equalsIgnoreCase("gm") && args.length > 0 && (args[0].equalsIgnoreCase("t") || args[0].equalsIgnoreCase("toggle"))) {
            if (args.length == 2) {
                if (isOnline(sender, args)) {
                    Consoletoggle(sender, args);
                }
            }
            if (args.length == 1) {
                log.info("[GameMode] Too few arguments!");
            }

        }


        if (args.length == 1 && label.equalsIgnoreCase("gms")) {
            if (isOnline(sender, args)) {
                dude.setGameMode(GameMode.SURVIVAL);

                dude.sendMessage(namn + ChatColor.DARK_RED + "Console" + ChatColor.GOLD + " changed your game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
                log.info("[GameMode] You changed " + dude.getDisplayName() + "'s game mode to Survival");
            }
        }

        if (args.length == 1 && label.equalsIgnoreCase("gmc")) {
            if (isOnline(sender, args)) {
                dude.setGameMode(GameMode.CREATIVE);

                dude.sendMessage(namn + ChatColor.DARK_RED + "Console" + ChatColor.GOLD + " changed your game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
                log.info("[GameMode] You changed " + dude.getDisplayName() + "'s game mode to Creative");
            }
        }
    }

    public void toggle(CommandSender sender) {
        Player player = (Player) sender;
        if (player.getGameMode().toString().equals("SURVIVAL")) {
            player.setGameMode(GameMode.CREATIVE);
            sender.sendMessage(namn + "You toggled your game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
            log.info("[GameMode] " + player.getDisplayName() + " toggled game mode to Creative!");
        } else {
            player.setGameMode(GameMode.SURVIVAL);
            sender.sendMessage(namn + "You toggled your game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
            log.info("[GameMode] " + player.getDisplayName() + " toggled game mode to Survival!");
        }
    }

    public void toggleother(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (dude.getGameMode().toString().equals("SURVIVAL")) {
            dude.setGameMode(GameMode.CREATIVE);
            sender.sendMessage(namn + "You changed " + dude.getDisplayName() + "'s game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
            dude.sendMessage(namn + ChatColor.DARK_RED + player.getDisplayName() + ChatColor.GOLD + " toggled your game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
            log.info("[GameMode] " + player.getDisplayName() + " toggled " + dude.getDisplayName() + "'s game mode to Creative!");

        } else {
            dude.setGameMode(GameMode.SURVIVAL);
            sender.sendMessage(namn + "You changed " + dude.getDisplayName() + "'s game mode  to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
            dude.sendMessage(namn + ChatColor.DARK_RED + player.getDisplayName() + ChatColor.GOLD + " toggled your game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
            log.info("[GameMode] " + player.getDisplayName() + " toggled " + dude.getDisplayName() + "'s game mode to Survival!");
        }
    }

    public void sendCHelp() {
        String name = "[GameMode] ";
        log.info("[GameMode] Help Page");
        log.info(name + "gm Commands:");
        log.info(name + "/gmc - Change to Creative mode");
        log.info(name + "/gms - Change to Survival mode");
        log.info(name + "/gmc <name> - Creative mode for <name>");
        log.info(name + "/gms <name> - Survival mode for <name>");
        log.info(name + "/gm t - Toggles between the two modes");
        log.info(name + "/gm t <name> - Toggles between the two modes for <name>");

    }

    public boolean sendHelp(Player player, String perm) {
        String perms;
		PluginDescriptionFile pdFile = this.getDescription();
		player.sendMessage("Game Mode Changer Help Version: " + ChatColor.RED + pdFile.getVersion() + ChatColor.WHITE + " By: " +  ChatColor.RED + "zachman1221");
        if(!player.isOp()){
        	if ("gm.creative".equals(perm)){
                player.sendMessage(namn + ChatColor.GREEN + "/gmc " + ChatColor.RED + "-" + ChatColor.GOLD + " Change to Creative mode");
        	}
        	if ("gm.survival".equals(perm)) {
                player.sendMessage(namn + ChatColor.GREEN + "/gms " + ChatColor.RED + "-" + ChatColor.GOLD + " Change to Survival mode");
        	}
        	if ("gm.creative.other".equals(perm)) {
                player.sendMessage(namn + ChatColor.GREEN + "/gmc <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Creative mode for <name>");
            }
            if ("gm.survival.other".equals(perm)) {
                player.sendMessage(namn + ChatColor.GREEN + "/gms <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Survival mode for <name>");
            }
            if ("gm.toggle".equals(perm)) {
                player.sendMessage(namn + ChatColor.GREEN + "/gm t " + ChatColor.RED + "-" + ChatColor.GOLD + " Toggles between the two modes");
            }
            if ("gm.toggle.other".equals(perm)) {
                player.sendMessage(namn + ChatColor.GREEN + "/gm t <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Toggles between the two modes for <name>");
            }
            	
        }
		return true;
    }
    
    public void Help(CommandSender sender) {
		PluginDescriptionFile pdFile = this.getDescription();
		sender.sendMessage("Game Mode Changer Version: " + ChatColor.RED + pdFile.getVersion() + ChatColor.WHITE + " By: " +  ChatColor.RED + "zachman1221");
		Object perm = null;
        if(!sender.isOp()){
    		sender.sendMessage(namn + ChatColor.GREEN + "/gms " + ChatColor.RED + "-" + ChatColor.GOLD + " Change to Survival mode");
            sender.sendMessage(namn + ChatColor.GREEN + "/gmc <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Creative mode for <name>");
            sender.sendMessage(namn + ChatColor.GREEN + "/gms <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Survival mode for <name>");
            sender.sendMessage(namn + ChatColor.GREEN + "/gm t " + ChatColor.RED + "-" + ChatColor.GOLD + " Toggles between the two modes");
            sender.sendMessage(namn + ChatColor.GREEN + "/gm t <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Toggles between the two modes for <name>");
            sender.sendMessage(namn + ChatColor.GREEN + "/gm c " + ChatColor.RED + "-" + ChatColor.GOLD + " Shows the changelog");	
        }
        else {
            if ("gm.creative".equals(perm)){
                   sender.sendMessage(namn + ChatColor.GREEN + "/gmc " + ChatColor.RED + "-" + ChatColor.GOLD + " Change to Creative mode");
           	}
           	if ("gm.survival".equals(perm)) {
                   sender.sendMessage(namn + ChatColor.GREEN + "/gms " + ChatColor.RED + "-" + ChatColor.GOLD + " Change to Survival mode");
           	}
           	if ("gm.creative.other".equals(perm)) {
               sender.sendMessage(namn + ChatColor.GREEN + "/gmc <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Creative mode for <name>");
            }
            if ("gm.survival.other".equals(perm)) {
               sender.sendMessage(namn + ChatColor.GREEN + "/gms <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Survival mode for <name>");
            }
            if ("gm.toggle".equals(perm)) {
               sender.sendMessage(namn + ChatColor.GREEN + "/gm t " + ChatColor.RED + "-" + ChatColor.GOLD + " Toggles between the two modes");
            }
            if ("gm.toggle.other".equals(perm)) {
                    sender.sendMessage(namn + ChatColor.GREEN + "/gm t <name> " + ChatColor.RED + "-" + ChatColor.GOLD + " Toggles between the two modes for <name>");
            }
        }
    }

    
    public void ChangeLog(CommandSender sender) {
		PluginDescriptionFile pdFile = this.getDescription();
		sender.sendMessage("Game Mode Changer Version: " + ChatColor.RED + pdFile.getVersion() + ChatColor.WHITE + " By: " +  ChatColor.RED + "zachman1221");
		sender.sendMessage("Changelog");
		sender.sendMessage(namn + "(+ Added)(- Removed)");
		sender.sendMessage(namn + ChatColor.GREEN + "3.20 " + ChatColor.RED + "-" + ChatColor.GOLD + " +Help only shows to people who have permission");
		sender.sendMessage(namn + ChatColor.GREEN + "3.16 " + ChatColor.RED + "-" + ChatColor.GOLD + " +Changlog -permission node help,change,change other");
		sender.sendMessage(namn + ChatColor.GREEN + "3.15 " + ChatColor.RED + "-" + ChatColor.GOLD + " +Change a players gamemode + toggle(t) gamemode");
        sender.sendMessage(namn + ChatColor.GREEN + "2.1 " + ChatColor.RED + "-" + ChatColor.GOLD + " Changed permission node");
        sender.sendMessage(namn + ChatColor.GREEN + "2.0 " + ChatColor.RED + "-" + ChatColor.GOLD + " +Permission Support");
        sender.sendMessage(namn + ChatColor.GREEN + "1.0 " + ChatColor.RED + "-" + ChatColor.GOLD + " Initial release!");
       }

    public void gm(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (args.length == 0 && hasPerms(player, "gm.help")) {
            Help(sender);
        }
        if (args.length == 1) {
            if ((args[0].equalsIgnoreCase("t") || args[0].equalsIgnoreCase("toggle")) && hasPerms(player, "gm.toggle")) {
                toggle(sender);
            }
            if ((args[0].equalsIgnoreCase("h") || args[0].equalsIgnoreCase("help"))) {
                Help(sender);
            }
            if ((args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("changelog"))) {
                ChangeLog(sender);
            }
        }
        if (args.length == 2 && hasPerms(player, "gm.toggle.other")) {
            if (args[0].equalsIgnoreCase("t")) {
                if (isOnline(sender, args)) {
                    toggleother(sender, args);
                }
            }
        }
    }


	public void Consoletoggle(CommandSender sender, String[] args) {
        if (dude.getGameMode().toString().equals("SURVIVAL")) {
            dude.setGameMode(GameMode.CREATIVE);
            dude.sendMessage(namn + ChatColor.DARK_RED + "Console" + ChatColor.GOLD + " toggled your game mode to " + ChatColor.GREEN + "Creative" + ChatColor.GOLD + "!");
            log.info("[GameMode] Console toggled " + dude.getDisplayName() + "'s game mode to Creative!");

        } else {
            dude.setGameMode(GameMode.SURVIVAL);
            dude.sendMessage(namn + ChatColor.DARK_RED + "Console" + ChatColor.GOLD + " toggled your game mode to " + ChatColor.GREEN + "Survival" + ChatColor.GOLD + "!");
            log.info("[GameMode] Console toggled " + dude.getDisplayName() + "'s game mode to Survival!");
        }
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        y = 0;
        x = 0;
        dude = null;
        found = false;
        if (!(sender instanceof Player)) {
            Console(sender, command, label, args);
        } else {
            if (args.length > 2) {
                sender.sendMessage(namn + "Ahhrrgg! Can't handle the amount of arguments!");
            } else {
                if (label.equalsIgnoreCase("gms")) {
                    Survival(sender, args);
                }
                if (label.equalsIgnoreCase("gm")) {
                    gm(sender, args);
                }
                if (label.equalsIgnoreCase("gmc")) {
                    Creative(sender, args);
                }

            }

            return true;
        }
        return true;
    }
}